import pygame

class TXGui:
	def __init__(self):
		self.mousePos = None
		self.lmbUP = False
		self._screen = None
		self.white = (255, 255, 255)
		self.black = (0, 0, 0)
		pass

	def UpdateGUI(self, screen_surf):
		self.mousePos = pygame.mouse.get_pos()
		self._screen = screen_surf

	def LMBClickUp(self):
		self.lmbUP = True

	def TextObj(self, text, font, color):
		textSurf = font.render(text, True, color)
		return textSurf, textSurf.get_rect()

	def TextShadow(self, text, font, x, y, w, h):
		color_gray = (131, 139, 139)
		shadowSurf, shadowRect = self.TextObj(text, font, color_gray)
		shadowRect.center = ( (x+(w/2)) + 2, (y+(h/2)) + 2 )
		return shadowSurf, shadowRect

	def Label(self, sText, x, y, font = None, fontsize = 20, fontcolor = (255, 255, 255)):
		if font == None:
			font = pygame.font.SysFont(pygame.font.get_default_font(), fontsize, False, False)

		textSurf, textRect = self.TextObj(sText, font, fontcolor)
		textRect = textRect.move(x, y)
		self._screen.blit(textSurf, textRect)

	def ButtonImg(self, sText, x, y, w, h, img, imgrect, callback = None, font = None, fontsize = 20, fontcolor = (255, 255, 255)):
		if font == None:
			font = pygame.font.SysFont(pygame.font.get_default_font(), fontsize, False, False)
		imgrect = imgrect.move(x, y)

		if x+w > self.mousePos[0] > x and y+h > self.mousePos[1] > y:
			#do hover stuff
			shadowSurf, shadowRect = self.TextShadow(sText, font, x, y, w, h)
			self._screen.blit(shadowSurf, shadowRect)
			self._screen.blit(img, imgrect)
			if self.lmbUP and callback != None:
				callback()
				self.lmbUP = False
		else:
			self._screen.blit(img, imgrect)

		textSurf, textRect = self.TextObj(sText, font, fontcolor)
		textRect.center = ( (x+(w/2)), (y+(h/2)) )
		self._screen.blit(textSurf, textRect)

	def Button(self, sText, x, y, w, h, color, hoverColor, callback = None, font = None, fontsize = 20, fontcolor = (255, 255, 255)):
		if font == None:
			font = pygame.font.SysFont(pygame.font.get_default_font(), fontsize, False, False)

		if x+w > self.mousePos[0] > x and y+h > self.mousePos[1] > y:
			#do hover stuff
			pygame.draw.rect(self._screen, hoverColor, (x,y,w,h))
			if self.lmbUP and callback != None:
				callback()
				self.lmbUP = False
		else:
			pygame.draw.rect(self._screen, color, (x,y,w,h))

		textSurf, textRect = self.TextObj(sText, font, fontcolor)
		textRect.center = ( (x+(w/2)), (y+(h/2)) )
		self._screen.blit(textSurf, textRect)

	#TODO: create a gui Edit for text output
	def EditFrame(self, sText, x, y, w, h, font = None, fontsize = 28, fontcolor = (255, 255, 255)):
		pass
