#!/usr/bin/python3

import pygame
from pygame.locals import *
import subprocess
import logging
import platform
from txgui import TXGui

red = (139, 0, 0)
bright_red = (255, 0, 0)
green = (0, 200, 0)
bright_green = (0, 255, 0)
black = (0, 0, 0)
white = (255, 255, 255)
gray = (131, 139, 139)

debugEnabled = True


def log(sText):
    if debugEnabled:
        logging.debug(sText)


font_path = None
wallpaperPath = None
aaLogoPath = None
kodiLogoPath = None
aaPath = None
exitLogoPath = None

gui = TXGui()


class App:
    def __init__(self):
        log("-> __init__")
        self._running = True
        self._screen = None
        self.size = self.wight, self.height = 800, 480
        self._leftMouseUp = False

    def on_init(self):
        log("-> on_init")
        pygame.init()
        if platform.system() == "Windows":
            self._screen = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        elif platform.system() == "Linux":
            self._screen = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.FULLSCREEN)
        self._running = True

        self._bgimg = pygame.image.load(wallpaperPath)
        self._bgrect = self._bgimg.get_rect()

        self.aa_logo = pygame.image.load(aaLogoPath)
        self.aa_logo = pygame.transform.scale(self.aa_logo, (55, 55))
        self.aa_rect = self.aa_logo.get_rect()

        self.kodi_logo = pygame.image.load(kodiLogoPath)
        self.kodi_logo = pygame.transform.scale(self.kodi_logo, (55, 55))
        self.kodi_rect = self.kodi_logo.get_rect()

        self.exit_logo = pygame.image.load(exitLogoPath)
        self.exit_logo = pygame.transform.scale(self.exit_logo, (55, 55))
        self.exit_rect = self.exit_logo.get_rect()

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                gui.LMBClickUp()

    def on_loop(self):
        self._screen.fill([255, 255, 255])
        self._screen.blit(self._bgimg, self._bgrect)
        gui.UpdateGUI(self._screen)

        gui.ButtonImg("Android Auto", 610, 75, 240, 55, self.aa_logo, self.aa_rect, self.RunAndroidAuto)
        gui.ButtonImg("Media player", 610, 205, 240, 55, self.kodi_logo, self.kodi_rect, self.RunKodi)
        gui.ButtonImg("Exit", 610, 340, 240, 55, self.exit_logo, self.exit_rect, self.Exit)

        pygame.display.update()
        self._leftMouseUp = False
        pass

    def on_cleanup(self):
        log("-> on_cleanup")
        pygame.display.quit()
        pygame.quit()

    def on_execute(self):
        if self.on_init() == False:
            self._running = False

        while (self._running):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()

        self.on_cleanup()

    ###########################################################################
    def RunAndroidAuto(self):
        log("AndroidAuto pressed!")
        if platform.system() == "Linux":
            subprocess.Popen(['sudo', aaPath])
        elif platform.system() == "Windows":
            print("AndroidAuto is running...")

    def RunKodi(self):
        log("Kodi pressed!")
        if platform.system() == "Linux":
            subprocess.Popen(['kodi'])
        elif platform.system() == "Windows":
            print("Kodi is running...")

    def Exit(self):
        log("Exit pressed!")
        self._running = False


# ========= ENTRY POINT =================================
if __name__ == "__main__":
    if platform.system() == "Windows":
        logging.basicConfig(filename='gui-logs.txt', level=logging.DEBUG)
        font_path = "Comic Sans MS"
        wallpaperPath = "data/grafix/wallpaper.png"
        aaLogoPath = "data/icons/icon_androidauto.png"
        kodiLogoPath = "data/icons/icon_kodi.png"
        aaPath = None
        exitLogoPath = "data/icons/icon_exit.png"
    elif platform.system() == "Linux":
        logging.basicConfig(filename='/home/pi/gui-logs.txt', level=logging.DEBUG)
        font_path = "Comic Sans MS"
        wallpaperPath = "/boot/polar/gui/data/grafix/wallpaper.png"
        aaLogoPath = "/boot/polar/gui/data/icons/icon_androidauto.png"
        kodiLogoPath = "/boot/polar/gui/data/icons/icon_kodi.png"
        exitLogoPath = "/boot/polar/gui/data/icons/icon_exit.png"
        aaPath = "/usr/local/bin/autoapp"

    theApp = App()
    theApp.on_execute()
